package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Calendar;
import java.util.Date;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {

    public static void transferInfo(WeatherResponse response, City cityInfo){
        cityInfo.setCloudiness(response.clouds.all);
        cityInfo.setHumidity(response.main.humidity);
        cityInfo.setTempKelvin(response.main.temp);
        cityInfo.setDescription(response.weather.get(0).description);
        cityInfo.setIcon(response.weather.get(0).icon);
        cityInfo.setWindSpeed(Math.round(response.wind.speed));
        cityInfo.setWindDirection(Math.round(response.wind.deg));
        long time = System.currentTimeMillis()/1000;
        cityInfo.setLastUpdate(time);
    }
}
