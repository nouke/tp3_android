package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

//api.openweathermap.org/data/2.5/weather?q=Avignon,FR&appid=77af22b80c4ab3279df6e3678897d463

public interface OWMInterface {

    String PATHS = "data/2.5/weather";

    @GET(PATHS)
    Call<WeatherResponse> getCityInfo(@Query("q") String q, @Query("appId") String appId);

}
